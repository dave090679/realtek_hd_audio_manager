#appModules/rthdcpl.py
#A part of NonVisual Desktop Access (NVDA)
#Copyright (C) 2006-2012 NVDA Contributors
#This file is covered by the GNU General Public License.
#See the file COPYING for more details.
import appModuleHandler
import api
import controlTypes
from NVDAObjects.IAccessible import IAccessible
class RTPageControl(IAccessible):
	def _get_name(self):
		return(self.firstChild.firstChild.windowText)

class AppModule(appModuleHandler.AppModule):
	def chooseNVDAObjectOverlayClasses(self, obj, clsList):
		if obj.windowClassName == 'TRzPageControl':
			clsList.insert(0, RTPageControl)

